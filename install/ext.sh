#!/bin/sh
set -e

PREFIX="${PREFIX:-$HOME/.local}"

script_dir="$(dirname "$0")"
cd "$script_dir/.."

echo "> Installing pamixer"
make -C ext/pamixer
make -C ext/pamixer install PREFIX="$PREFIX"

echo "> Installing oblogout"
old_pwd="$PWD"; cd ext/oblogout 
./setup.py install --user
cd "$old_pwd"

echo "> Installing ui-tils"
go get $(cat - <<EOF
gitlab.com/subnixr/runr
gitlab.com/subnixr/ui-tils/...
EOF
)
