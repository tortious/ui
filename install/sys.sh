#!/bin/sh
set -e

apt-get install $(cat - <<EOF
acpi
arandr
bc
blueman
compton
curl
dex
dunst
feh
ffmpeg
fontconfig
g++
golang
gsimplecal
htop
i3
ibus
jq
libboost1.67-dev
libboost-program-options1.67-dev
libnotify-bin
libpulse-dev
lxpolkit
make
network-manager-gnome
parcellite
pkg-config
psmisc
pulseaudio
python3
python3-pil
python3-pip
python3-xdg
python-dbus
python-distutils-extra
python-pil
qt5ct
qutebrowser
redshift
rofi
rxvt-unicode
slop
tar
tree
unzip
xorg
xsel
xss-lock
zlib1g-dev
EOF
)

LIBSASS0_URL="http://ftp.us.debian.org/debian/pool/main/libs/libsass/libsass0_3.4.3-1_arm64.deb"
OOMOX_URL="https://github.com/themix-project/oomox/releases/download/1.7.0.4/oomox_1.7.0.4.deb"
echo "> Installing oomox"
curl -L# "$LIBSASS0_URL" -o /tmp/libsass0.deb
curl -L# "$OOMOX_URL" -o /tmp/oomox.deb
apt-get install /tmp/libsass0.deb /tmp/oomox.deb

