#!/bin/sh
set -e

HERMIT_URL="https://pcaro.es/d/otf-hermit-1.21.tar.gz"
OVERPASS_URL="https://github.com/RedHatBrand/overpass/archive/3.0.2.tar.gz"
FONTAWESOME_URL="https://use.fontawesome.com/releases/v5.0.9/fontawesome-free-5.0.9.zip"

WALLPAPER_URL="https://i.imgur.com/6cHeAzC.jpg"
LOCK_URL="https://i.imgur.com/ACx1lUQ.png"

PAPER_ICONS_URL="https://github.com/snwh/paper-icon-theme/archive/v1.4.0.tar.gz"

echo "> Installing Fonts"
mkdir -p "$HOME/.local/share/fonts"

curl -L# "$HERMIT_URL" -o /tmp/hermit.tar.gz
mkdir -p "$HOME/.local/share/fonts/hermit"
tar -xvzf /tmp/hermit.tar.gz -C "$HOME/.local/share/fonts/hermit"

curl -L# "$OVERPASS_URL" -o /tmp/overpass.tar.gz
tar -xvzf /tmp/overpass.tar.gz -C /tmp Overpass-3.0.2/desktop-fonts
cp -r /tmp/Overpass-3.0.2/desktop-fonts/* -t "$HOME/.local/share/fonts"

curl -L# "$FONTAWESOME_URL" -o /tmp/fontawesome.zip
unzip /tmp/fontawesome.zip -d /tmp
mkdir -p "$HOME/.local/share/fonts/fontawesome"
cp -r /tmp/fontawesome-free-5.0.9/use-on-desktop/* -t "$HOME/.local/share/fonts/fontawesome"

fc-cache -rf

echo "> Installing wallpaper, lockscreen"
curl -L# --create-dirs "$WALLPAPER_URL" \
    -o "$HOME/.local/share/pixmaps/wallpaper.png"
curl -L# --create-dirs "$LOCK_URL" \
    -o "$HOME/.local/share/pixmaps/lock.png"

echo "> Installing icons"
mkdir -p "$HOME/.local/share/icons"
curl -L# "$PAPER_ICONS_URL" -o /tmp/paper-icons.tar.gz
tar -xvzf /tmp/paper-icons.tar.gz -C /tmp paper-icon-theme-1.4.0/Paper
cp -rv /tmp/paper-icon-theme-1.4.0/Paper "$HOME/.local/share/icons"

