#!/bin/sh
set -e

script_dir="$(dirname "$0")"
cd "$script_dir"

echo "> Linking configuration files"
find cfg/ -type f -printf "%h/%f\n" | while read file; do
    local_file="$HOME/$(echo "$file" | cut -d/ -f2-)"
    mkdir -p "$(dirname "$local_file")"
    echo "$PWD/$file -> $local_file"
    ln -sf "$PWD/$file" "$local_file"
done

