menu() {
    dmenu="${dmenu:-rofi -dmenu -sync -i}"
    menu="$dmenu -p '$prompt'"
    tty >/dev/null && menu="fzf"

    sel="$(eval $1 | eval "$menu")"
    [ -n "$sel" ] && eval $2 "$sel"
}
