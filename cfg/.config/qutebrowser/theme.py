COLOR_BGDARK = "#2E3440"
COLOR_BGINACT = "#3b4252"
COLOR_BGLIGHT = "#4C566A"

COLOR_FGDARK = "#D8DEE9"
COLOR_FGINACT = "#88C0D0"
COLOR_FGLIGHT = "#ECEFF4"

COLOR_ACCENT = "#5E81AC"

COLOR_OK = "#A3BE8C"
COLOR_WARNING = "#EBCB8B"
COLOR_URGENT="#BF616A"

c.colors.completion.category.fg = COLOR_FGLIGHT
c.colors.completion.category.bg = COLOR_BGDARK
c.colors.completion.category.border.bottom = COLOR_BGDARK
c.colors.completion.category.border.top = COLOR_BGDARK

c.colors.completion.fg = COLOR_FGDARK
c.colors.completion.even.bg = COLOR_BGDARK
c.colors.completion.odd.bg = COLOR_BGDARK

c.colors.completion.item.selected.fg = COLOR_FGLIGHT
c.colors.completion.item.selected.bg = COLOR_BGLIGHT
c.colors.completion.item.selected.border.bottom = COLOR_BGLIGHT
c.colors.completion.item.selected.border.top = COLOR_BGLIGHT

c.colors.completion.match.fg = COLOR_ACCENT

c.colors.completion.scrollbar.fg = COLOR_FGDARK
c.colors.completion.scrollbar.bg = COLOR_BGDARK

c.colors.hints.bg = COLOR_BGDARK
c.colors.hints.fg = COLOR_FGDARK
c.colors.hints.match.fg = COLOR_ACCENT

c.colors.messages.error.bg = COLOR_URGENT
c.colors.messages.error.border = COLOR_URGENT
c.colors.messages.error.fg = COLOR_FGLIGHT

c.colors.messages.info.bg = COLOR_BGDARK
c.colors.messages.info.border = COLOR_BGDARK
c.colors.messages.info.fg = COLOR_FGDARK

c.colors.messages.warning.bg = COLOR_WARNING
c.colors.messages.warning.border = COLOR_WARNING
c.colors.messages.warning.fg = COLOR_FGDARK

c.colors.statusbar.url.fg = COLOR_FGDARK
c.colors.statusbar.url.error.fg = COLOR_URGENT
c.colors.statusbar.url.hover.fg = COLOR_ACCENT
c.colors.statusbar.url.success.http.fg = COLOR_FGDARK
c.colors.statusbar.url.success.https.fg = COLOR_OK
c.colors.statusbar.url.warn.fg = COLOR_WARNING

c.colors.statusbar.normal.fg = COLOR_FGDARK
c.colors.statusbar.normal.bg = COLOR_BGDARK

c.colors.statusbar.private.fg = COLOR_FGLIGHT
c.colors.statusbar.private.bg = COLOR_ACCENT

c.colors.statusbar.command.fg = COLOR_FGDARK
c.colors.statusbar.command.bg = COLOR_BGDARK

c.colors.statusbar.insert.fg = COLOR_FGDARK
c.colors.statusbar.insert.bg = COLOR_ACCENT

c.colors.statusbar.passthrough.fg = COLOR_FGDARK
c.colors.statusbar.passthrough.bg = COLOR_ACCENT

# TODO
c.colors.statusbar.command.private.fg = COLOR_FGDARK
c.colors.statusbar.command.private.bg = COLOR_ACCENT

c.colors.statusbar.progress.bg = COLOR_FGLIGHT

c.colors.tabs.bar.bg = COLOR_BGINACT
c.colors.tabs.even.fg = COLOR_FGDARK
c.colors.tabs.even.bg = COLOR_BGINACT
c.colors.tabs.odd.fg = COLOR_FGDARK
c.colors.tabs.odd.bg = COLOR_BGINACT

c.colors.tabs.selected.even.fg = COLOR_FGLIGHT
c.colors.tabs.selected.even.bg = COLOR_BGLIGHT
c.colors.tabs.selected.odd.fg = COLOR_FGLIGHT
c.colors.tabs.selected.odd.bg = COLOR_BGLIGHT

c.colors.tabs.indicator.error = COLOR_URGENT
c.colors.tabs.indicator.start = COLOR_ACCENT
c.colors.tabs.indicator.stop = COLOR_OK

c.colors.downloads.bar.bg = COLOR_BGLIGHT

c.colors.downloads.stop.fg = COLOR_FGLIGHT
c.colors.downloads.stop.bg = COLOR_OK

c.colors.downloads.start.fg = COLOR_FGLIGHT
c.colors.downloads.start.bg = COLOR_ACCENT

c.colors.downloads.error.fg = COLOR_FGLIGHT
c.colors.downloads.error.bg = COLOR_URGENT


c.fonts.monospace = '"xos4 Terminus", Terminus, Monospace, "DejaVu Sans Mono", Monaco, "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", Courier, "Liberation Mono", monospace, Fixed, Consolas, Terminal'

c.fonts.completion.category = 'bold 11pt Overpass'
c.fonts.completion.entry = '11pt Overpass'
c.fonts.debug_console = '11pt Overpass'
c.fonts.downloads = '11pt Overpass'
c.fonts.hints = 'bold 11pt Overpass'
c.fonts.keyhint = '11pt Overpass'
c.fonts.messages.error = '11pt Overpass'
c.fonts.messages.info = '11pt Overpass'
c.fonts.messages.warning = '11pt Overpass'
c.fonts.prompts = '11pt Overpass'
c.fonts.statusbar = '11pt Overpass'
c.fonts.tabs = '11pt Overpass'

c.hints.border = '1px solid ' + COLOR_ACCENT

