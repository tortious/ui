## Documentation:
##   qute://help/configuring.html
##   qute://help/settings.html
import os

config.load_autoconfig()

config.source("theme.py")

c.aliases["q"] = "tab-close"
c.aliases["qa"] = "close"
c.aliases["mpv"] = "spawn --detach mpv --force-window yes {url}"
# gotta fix ytmp3
c.aliases["ytmp3"] = "spawn --detach ytmp3 {url}"

config.bind("X", "undo")
config.bind("x", "tab-close")

config.bind("gt", "tab-next")
config.bind("gT", "tab-prev")

config.bind("<Space>", "scroll-page 0 0.75")
config.bind("<Shift-Space>", "scroll-page 0 -0.75")

c.input.insert_mode.auto_load = True

c.url.start_pages = 'file://' + os.environ['HOME'] + '/.local/share/startpage/index.html'
c.url.default_page = 'file://' + os.environ['HOME'] + '/.local/share/startpage/index.html'

c.hints.mode = 'number'
c.hints.auto_follow = 'full-match'

c.tabs.background = True
c.new_instance_open_target = 'window'
# c.tabs.tabs_are_windows = True

c.window.title_format = '{perc}{title}'

c.content.cookies.store = False

