#!/bin/sh
set -e

script_dir="$(dirname "$0")"
cd "$script_dir"

export PREFIX="$HOME/.local"

sudo ./install/sys.sh
./install/ext.sh
./install/res.sh
./relink.sh

echo "> Generate oomox theme"
oomox-cli -o oomox-vandal "$HOME/.config/oomox/colors/vandal"

